package utility_common_methods;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel_data_extractor {

	public static ArrayList<String> Excel_data_reader(String filename, String sheetname, String tc_name)
			throws IOException {
		ArrayList<String> Arraydata=new ArrayList<String>(); 
		String project_dir = System.getProperty("user.dir");

		// Step 1 create the object of file input stream to locate the data file
		FileInputStream fis = new FileInputStream(project_dir + "\\Data_Files\\Test_Data.xlsx");

		// Step 2 Create the XSSFWorkbook object to open the excel file
		XSSFWorkbook xs = new XSSFWorkbook(fis);

		// Step 3 fetch the number of sheets available in the excel file
		int count = xs.getNumberOfSheets();

		// Step 4 access the sheet as per the input sheet name

		for (int i = 0; i < count; i++) {
			String SheetName = xs.getSheetName(i);

			if (SheetName.equals(sheetname)) {
				System.out.println(SheetName);
				XSSFSheet Sheet = xs.getSheetAt(i);
				Iterator<Row> row = Sheet.iterator();
				row.next();
				while (row.hasNext()) {
					Row data_row = row.next();
					String TC_name = data_row.getCell(0).getStringCellValue();
					if (TC_name.equals(tc_name)) {
						// System.out.println(TC_name);
						Iterator<Cell> cellvalues = data_row.iterator();
						while (cellvalues.hasNext()) {
							String testdata = cellvalues.next().getStringCellValue();
							//System.out.println(testdata);
							 Arraydata.add(testdata);

						}

					}
				}
				break;
			}

		}
		xs.close();
		return Arraydata;
	

	}
}
