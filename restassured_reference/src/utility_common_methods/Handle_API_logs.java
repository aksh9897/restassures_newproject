package utility_common_methods;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Handle_API_logs {

	public static void evidence_creator(File dir_name, String file_name, String endpoint, String requestbody,
			String responsebody) throws IOException {
		File newfile = new File(dir_name + "\\" + file_name + ".txt");
		System.out.println("To save request and response body we have created a new file named : " + newfile.getName());
		FileWriter datawriter = new FileWriter(newfile);
		datawriter.write("endpoint body is :" + endpoint + "\n\n");
		datawriter.write("Request body is :" + requestbody + "\n\n");
		datawriter.write("response body is :" + responsebody + "\n\n");
		datawriter.close();
	}
}
