package Test_Case_Packg;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import API_common_Methods.Common_Method_Handle_API;
import Endpoint.Put_endpoint;
import Request_repository.Put_request_repository;
import io.restassured.path.json.JsonPath;
import utility_common_methods.Handle_API_logs;
import utility_common_methods.Handle_directory;

public class Put_TestCase extends Common_Method_Handle_API {
@Test
	public static void excutor() throws IOException {
		File log_dir = Handle_directory.Create_log_directory("Put_TestCase_logs");
		String requestbody = Put_request_repository.Put_request_repository_TC2();
		String endpoint = Put_endpoint.Put_endpoint_TC2();
		for (int i = 0; i < 5; i++) {
			int statuscode = Put_statusCode(requestbody, endpoint);
			System.out.println(statuscode);
			if (statuscode == 201) {
				String responsebody = Put_responsebody(requestbody, endpoint);
				System.out.println(responsebody);
				Handle_API_logs.evidence_creator(log_dir, "Put_TestCase", endpoint, requestbody, responsebody);
				Put_TestCase.validator(requestbody, responsebody);
				break;
			} else {
				System.out.println("expected status code not found hence retrying");
			}
		}
	}

	public static void validator(String requestbody, String responsebody) {
		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		JsonPath jsp_res = new JsonPath(responsebody);
		String res_name = jsp_res.getString("name");
		System.out.println("name is :" + res_name);
		String res_job = jsp_res.getString("job");
		System.out.println("job is :" + res_job);
		String res_createdAt = jsp_res.getString("createdAt");
		System.out.println("createdAt is :" + res_createdAt);
		res_createdAt = res_createdAt.substring(0, 11);

//STEP 4 validate reponsebody
		Assert.assertEquals(res_name, "morpheus");
		Assert.assertEquals(res_job, "zion resident");
		Assert.assertEquals(res_createdAt, expecteddate);

	}
}
