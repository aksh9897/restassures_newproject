package Driver_Packg;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import utility_common_methods.Excel_data_extractor;

public class Dynamic_driver_class {

	public static void main(String[] args) throws IOException, ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		ArrayList<String> TC_Excute=Excel_data_extractor.Excel_data_reader("Test_Data", "Test_Cases", "TC_Name");
		System.out.println(TC_Excute);
		int count=TC_Excute.size();
		for(int i=1;i<count;i++) {
			String TC_Name=TC_Excute.get(i);
			System.out.println(TC_Name);
			// Call the testcaseclass on runtime by using java.lang.reflect package
			Class<?> Test_class=Class.forName("Test_Case_Packg." +TC_Name);
			// Call the execute method belonging to test class captured in variable TC_name by using java.lang.reflect.method class
			Method execute_method=Test_class.getDeclaredMethod("excutor");		
			// Set the accessibility of method true
			execute_method.setAccessible(true);
			// Create the instance of testclass captured in variable name testclassname
			
			Object instance_of_test_class=Test_class.getDeclaredConstructor().newInstance();
					
			//Execute the test script class fetched in variable Test_class
			execute_method.invoke(instance_of_test_class);
						


		}

	}

}
