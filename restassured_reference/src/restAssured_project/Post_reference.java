package restAssured_project;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;


public class Post_reference {

	public static void main(String[] args) {
		// step 1 declared base URL

				RestAssured.baseURI = "https://reqres.in/";

				// step 2 configure the request parameters and trigger the API
				String requestbody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"leader\"\r\n" + "}";
				String responsebody = given().header("Content-Type", "application/json")
						.body(requestbody).when()
						.post("api/users").then().log().all().extract().response().asString();
				// System.out.println("responsebody is : " +responsebody );

				// step3 create the object of json path to parse the requestbody and then the response body
				JsonPath jsp_req = new JsonPath(requestbody);
				String req_name = jsp_req.getString("name");
				String req_job = jsp_req.getString("job");
				LocalDateTime currentdate = LocalDateTime.now();
				String expecteddate = currentdate.toString().substring(0, 11);
				JsonPath jsp_res = new JsonPath(responsebody);
				String res_id = jsp_res.getString("id");
				System.out.println("id is :" + res_id);
				String res_name = jsp_res.getString("name");
				System.out.println("name is :" + res_name);

				String res_job = jsp_res.getString("job");
				System.out.println("job is :" + res_job);

				String res_createdAt = jsp_res.getString("createdAt");
				System.out.println("createdAt is :" + res_createdAt); 
				res_createdAt=res_createdAt.substring(0,11);

				// step 4 validate the response
			//	Assert.assertEquals(res_name, "morpheus");
			//	Assert.assertEquals(res_job, "leader");
			//	Assert.assertNotNull(res_id);
			//	Assert.assertEquals(res_createdAt, expecteddate);

	}

}
