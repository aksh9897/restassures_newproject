package restAssured_project;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

public class Put_reference {

	public static void main(String[] args) {
		//STEP 1 declare the URL
				RestAssured.baseURI="https://reqres.in/";
				
			// STEP 2 configure the request parameters and trigger the api
				String requestbody="{\r\n"
						+ "    \"name\": \"morpheus\",\r\n"
						+ "    \"job\": \"zion resident\"\r\n"
						+ "}";
				String responsebody = given().header("Content-Type", "application/json").body(requestbody).
						when().put("api/users/2").then().log().all().extract().response().asString();
				System.out.println("the response is :" +responsebody);
				
						JsonPath jsp_req = new JsonPath(requestbody);
						String req_name=jsp_req.getString("name");
						String req_job=jsp_req.getString("job");
						LocalDateTime currentdate=LocalDateTime.now();
						String expecteddate=currentdate.toString().substring(0,11);
						
						JsonPath jsp_res=new JsonPath(responsebody);
						String res_name=jsp_res.getString("name");
						System.out.println("name is :" + res_name);
						String res_job = jsp_res.getString("job");
						System.out.println("job is :" + res_job);
						String res_updatedAt = jsp_res.getString("updatedAt");
						System.out.println("updatedAt is :" + res_updatedAt);
						res_updatedAt = res_updatedAt.substring(0, 11);

						
					//	Assert.assertEquals(res_name, "morpheus");
					//	Assert.assertEquals(res_job, "zion resident");
					//	Assert.assertEquals(res_updatedAt,expecteddate );

	}

}
